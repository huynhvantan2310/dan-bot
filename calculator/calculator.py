class User_calculator:
    def __init__(self, user_data):
        self.age_step = user_data.age_step
        self.exercice_step = user_data.exercice_step
        self.evaluate_health = user_data.evaluate_health
        self.capacity_activities = user_data.capacity_activities
        self.key_goals = user_data.key_goals
        self.injuries = user_data.injuries
        self.day_exercice = user_data.day_exercice
        self.meditation = user_data.meditation
        self.count_water_remind = user_data.count_water_remind
        self.count_posture_remind = user_data.count_posture_remind
        self.time_go_home = user_data.time_go_home

    def get_results(self):
        # Calculate health point and predict by score
        point = self.exercice_step.index + self.evaluate_health.index + self.capacity_activities.index + 1

        point_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        status_list = ['low', 'low', 'low', 'low', 'moderate', 'moderate', 'moderate',
                       'good', 'good', 'good', 'excellent', 'excellent', 'excellent', ]

        status = status_list[point_list[point]]
        msg = "Your score is {}. That's {} health !!".format(point, status)
        return msg
