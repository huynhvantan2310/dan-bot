# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

from botbuilder.dialogs import (
    ComponentDialog,
    WaterfallDialog,
    WaterfallStepContext,
    DialogTurnResult,
)
from botbuilder.dialogs.prompts import (
    TextPrompt,
    NumberPrompt,
    ChoicePrompt,
    ConfirmPrompt,
    PromptOptions,
)
from botbuilder.dialogs.choices import Choice
from botbuilder.core import MessageFactory, UserState

from data_models import UserProfile
from calculator import User_calculator


class UserProfileDialog(ComponentDialog):
    def __init__(self, user_state: UserState):
        super(UserProfileDialog, self).__init__(UserProfileDialog.__name__)

        self.user_profile_accessor = user_state.create_property("UserProfile")

        self.add_dialog(
            WaterfallDialog(
                WaterfallDialog.__name__,
                [
                    self.age_step,
                    self.exercice_step,
                    self.evaluate_health,
                    self.capacity_activities,
                    self.key_goals,
                    self.injuries,
                    self.day_exercice,
                    self.water_remind,
                    self.count_water_remind,
                    self.posture,
                    self.count_posture_remind,
                    self.go_home,
                    self.time_go_home,
                    self.meditation,
                    self.summary_step,
                ],
            )
        )
        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(NumberPrompt(NumberPrompt.__name__))
        self.add_dialog(ChoicePrompt(ChoicePrompt.__name__))
        self.add_dialog(ConfirmPrompt(ConfirmPrompt.__name__))
        self.initial_dialog_id = WaterfallDialog.__name__

    async def age_step(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        # WaterfallStep always finishes with the end of the Waterfall or with another dialog;
        # here it is a Prompt Dialog. Running a prompt here means the next WaterfallStep will
        # be run when the users response is received.
        return await step_context.prompt(
            ChoicePrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("Hi, I'm Bot of NANO New Amazing Nerdy Opponents Team."
                                           " I will advice and give recommendation for your health."
                                           " Please answer question, how old are you ?"),
                choices=[Choice("15-20"), Choice("21-30"), Choice("31-40"),
                         Choice("41-50"), Choice("51-60"), Choice("61+")],
            ),
        )

    async def exercice_step(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        step_context.values["age_step"] = step_context.result

        return await step_context.prompt(
            ChoicePrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("How often do you exercice ?"),
                choices=[Choice("Not at all (0 per month or year)"),
                         Choice("Rarely (May or may have exercise in a month or quarter)"),
                         Choice("Regular (1 - 3 times a week)"),
                         Choice("Consistent ( 3 or 4 times a week )"),
                         Choice("Athletic (5 - 6 times a week)")],
            ),
        )

    async def evaluate_health(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        step_context.values["exercice_step"] = step_context.result

        return await step_context.prompt(
            ChoicePrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("How would you evaluate your overall health?"),
                choices=[Choice("Poor"),
                         Choice("Regular"),
                         Choice("Good"),
                         Choice("Very Good"),
                         Choice("Exellent")],
            ),
        )

    async def capacity_activities(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        step_context.values["evaluate_health"] = step_context.result

        return await step_context.prompt(
            ChoicePrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("In your opinion, what is your capacity in activities / exercices?"),
                choices=[Choice("Very low capacity"),
                         Choice("Low capacity"),
                         Choice("Moderate capacity"),
                         Choice("Good capacity"),
                         Choice("Excellent capacity")],
            ),
        )

    async def key_goals(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        step_context.values["capacity_activities"] = step_context.result

        return await step_context.prompt(
            ChoicePrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("What are your key goals ?"),
                choices=[Choice("Look more muscular"),
                         Choice("Have more energy"),
                         Choice("Improve mobility/flexibility"),
                         Choice("Sleep better"),
                         Choice("Weight management"),
                         Choice("Feel more confident"),
                         Choice("Build strength"),
                         Choice("Improve overall health")],
            ),
        )

    async def injuries(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        step_context.values["key_goals"] = step_context.result

        return await step_context.prompt(
            ChoicePrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("Do you have some injuries ?"),
                choices=[Choice("Hand"),
                         Choice("Wrist"),
                         Choice("Arm"),
                         Choice("Leg"),
                         Choice("Back"),
                         Choice("Head"),
                         Choice("None")],
            ),
        )

    async def day_exercice(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        step_context.values["injuries"] = step_context.result

        return await step_context.prompt(
            ChoicePrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("Which day(s) you want to exercice ?"),
                choices=[Choice("Monday"),
                         Choice("Tuesday"),
                         Choice("Wednesday"),
                         Choice("Thursday"),
                         Choice("Friday"),
                         Choice("Saturday"),
                         Choice("None")],
            ),
        )

    async def water_remind(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        step_context.values["day_exercice"] = step_context.result
        return await step_context.prompt(
            ConfirmPrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("Drinking water does more than just quench your thirst — it’s essential "
                                           "to keeping your body functioning properly and feeling healthy. Nearly all"
                                           " of your body’s major systems depend on water to function and survive."
                                           " You’d be surprised about what staying hydrated can do for your body. "
                                           "Daily water reminder ?")
            ),
        )

    async def count_water_remind(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        if step_context.result:
            return await step_context.prompt(
                ChoicePrompt.__name__,
                PromptOptions(
                    prompt=MessageFactory.text("How many reminders per day ?"),
                    choices=[Choice("None"),
                             Choice("1"),
                             Choice("2"),
                             Choice("3"),
                             Choice("4"),
                             Choice("5+")],
                ),
            )

        return await step_context.next(-1)

    async def posture(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        step_context.values["count_water_remind"] = step_context.result

        return await step_context.prompt(
            ConfirmPrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("Maintaining good posture all day requires conscious effort. Most employees,"
                                           " who use computers, stare into their screen for hours and slowly get"
                                           " drawn into it. This means they stretch their neck forward, which puts"
                                           " pressure on the neck and the spin. Maintain good posture is important"
                                           " for your body and health. Daily maintain good posture reminder ?")
            ),
        )

    async def count_posture_remind(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        if step_context.result:
            step_context.values["posture"] = step_context.result
            return await step_context.prompt(
                ChoicePrompt.__name__,
                PromptOptions(
                    prompt=MessageFactory.text("How many reminders per day ?"),
                    choices=[Choice("None"),
                             Choice("1"),
                             Choice("2"),
                             Choice("3"),
                             Choice("4"),
                             Choice("5+")],
                ),
            )

        return await step_context.next(-1)

    async def go_home(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        step_context.values["count_posture_remind"] = step_context.result

        return await step_context.prompt(
            ConfirmPrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("Do you overtime everyday ? Set up an exclusive reminder to go home and"
                                           " take some rest. Time to go home reminder ?")
            ),
        )

    async def time_go_home(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        if step_context.result:
            step_context.values["go_home"] = step_context.result
            return await step_context.prompt(
                ChoicePrompt.__name__,
                PromptOptions(
                    prompt=MessageFactory.text("Time set up"),
                    choices=[Choice("17:00 PM"),
                             Choice("17:30 PM"),
                             Choice("18:00 PM"),
                             Choice("18:30 PM"),
                             Choice("19:00 PM"),
                             Choice("19:30 PM"),
                             Choice("20:00 PM"),
                             Choice("20:30 PM"),
                             Choice("21:00 PM")],
                ),
            )
        return await step_context.next(-1)

    async def meditation(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        step_context.values["time_go_home"] = step_context.result

        return await step_context.prompt(
            ConfirmPrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("Most of us lead hectic lifes that can get to us from time to time."
                                           " Juggling several roles and responsibilities on a daily basis can leave"
                                           " us exhausted, which is normal. After all, we’re only human. However,"
                                           " we can make it easier on ourselves by learning to manage stress."
                                           " One great way to beat stress? Meditate. Get meditation sessions ?")
            ),
        )

    async def summary_step(
            self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:

        step_context.values["meditation"] = step_context.result
        if step_context.result:
            # Get the current profile object from user state.  Changes to it
            # will saved during Bot.on_turn.
            user_profile = await self.user_profile_accessor.get(
                step_context.context, UserProfile
            )

            user_profile.age_step = step_context.values["age_step"]
            user_profile.exercice_step = step_context.values["exercice_step"]
            user_profile.evaluate_health = step_context.values["evaluate_health"]
            user_profile.capacity_activities = step_context.values["capacity_activities"]
            user_profile.key_goals = step_context.values["key_goals"]
            user_profile.injuries = step_context.values["injuries"]
            user_profile.day_exercice = step_context.values["day_exercice"]
            user_profile.meditation = step_context.values["meditation"]
            user_profile.count_water_remind = step_context.values["count_water_remind"]
            user_profile.count_posture_remind = step_context.values["count_posture_remind"]
            user_profile.time_go_home = step_context.values["time_go_home"]

            Calculator = User_calculator(user_profile)
            msg = Calculator.get_results()

            await step_context.context.send_activity(MessageFactory.text(msg))
        else:
            await step_context.context.send_activity(
                MessageFactory.text("Thanks. Your profile will not be kept.")
            )

        return await step_context.end_dialog()
