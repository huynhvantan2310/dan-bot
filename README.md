# CHATBOT FOR GENERATE YOUR HEALTH STATUS
## _NANO New Amazing Nerdy Opponents - Hackathon_


This is project to submit for data-hackrathon 2021 (Ubisoft Global)

## 1. Features

- Create conversation with users
- Give question and reveice answer to calculate point and put them in a profile, collect health information from user to generate the right set of exercise for user at work.
- Embbeding to Ubisoft's Microsoft Teams (Still in progress)
- Set reminder for users to drink water and have the right posture while working (Still in progress)

![GitHub Logo](images/Flow.png)

## 2. Tech

Dillinger uses a number of open source projects to work properly:

- [Bot Frame Work SDK For Python] - Bot provide an experience that feels less like using a computer and more like deadling with a persion - or at least an intelligent robot.


## 3. Prequisites

- Python > 3.6
- Pip library (Reference : https://phoenixnap.com/kb/install-pip-windows)


## 4. Running Chatbot:

Clone this code to your PC
```sh
git clone https://gitlab.com/huynhvantan2310/dan-bot
```

Install requirement library
```sh
cd dan-bot
pip install -r requirement.txt
```

## 5. Running app:

```sh
python app.py
```

## 6. Using bot-framework:

Reference : Bot framework emulator (https://github.com/Microsoft/BotFramework-Emulator/releases)

Connect to chatbot API running in step 1.

## 7. Example conversation:

![](images/Conversation1.png)

![](images/Conversation2.png)

![](images/Conversation3.png)

## 8. Contact

Email - [diep.tran-ngoc@ubisoft.com](diep.tran-ngoc@ubisoft.com)

Email - [angelina.sharipova@ubisoft.com](angelina.sharipova@ubisoft.com)

Email - [mark.lucido@ubisoft.com](mark.lucido@ubisoft.com)

Email - [huy.ha-tu@ubisoft.com](huy.ha-tu@ubisoft.com)

Email - [tan.huynh-van@ubisoft.com.vn](tan.huynh-van@ubisoft.com.vn)

