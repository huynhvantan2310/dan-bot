# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

from botbuilder.schema import Attachment


class UserProfile:
    """
      This is our application state. Just a regular serializable Python class.
    """

    def __init__(self, age_step: int = None, exercice_step: str = None, evaluate_health: str = None,
                 capacity_activities: str = None, key_goals: str = None, injuries: str = None,
                 day_exercice: str = None, count_water_remind: str = None, count_posture_remind: str = None,
                 time_go_home: str = None, meditation: str = None):
        self.age_step = age_step
        self.exercice_step = exercice_step
        self.evaluate_health = evaluate_health
        self.capacity_activities = capacity_activities
        self.key_goals = key_goals
        self.injuries = injuries
        self.day_exercice = day_exercice
        self.count_water_remind = count_water_remind
        self.count_posture_remind = count_posture_remind
        self.time_go_home = time_go_home
        self.meditation = meditation

